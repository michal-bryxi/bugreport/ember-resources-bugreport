import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default class ApplicationController extends Controller {
  @service settings;

  get companyName() {
    console.log('before requesting companyName');
    console.log(this.settings?.companyResource?.company?.name);
    console.log('after requesting companyName');
    return this.settings?.companyResource?.company?.name;
  }
}
