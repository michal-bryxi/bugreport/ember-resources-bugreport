import { LifecycleResource } from 'ember-resources';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import { cached } from '@glimmer/tracking';

export default class CompanyResource extends LifecycleResource {
  @service store;
  @tracked _company;

  // TODO[comment / uncomment this]
  @cached
  get company() {
    console.log('company');
    return this._company ?? {};
  }

  setup() {
    this.doAsyncTask();
  }

  update() {
    this.doAsyncTask();
  }

  async doAsyncTask() {
    console.log('before');
    const tmp = await this.store.findAll('company'); // weirdness of the API, don't ask
    this._company = tmp.firstObject;
    console.log('after');
  }
}
