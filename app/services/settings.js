import Service from '@ember/service';
import { useResource } from 'ember-resources';
import CompanyResource from '../resources/company';

export default class SettingsService extends Service {
  companyResource = useResource(
    this,
    CompanyResource /* no thunk, no call to update */
  );
}
